﻿using System.Collections;
using System.Diagnostics;

namespace CircularSinglyLinkedList;

public class Node<T>
{
    public Node(T data)
    {
        Data = data;
    }
    public T Data { get; }
    public Node<T>? Next { get; set; }
}

public class CircularSinglyLinkedList<T> : IEnumerable<T>
    {
        private Node<T>? _head;
        private Node<T>? _tail;

        public void Add(T data)
        {
            var node = new Node<T>(data);
            if (_head == null)
            {
                _head = node;
                _tail = node;
                _tail.Next = _head;
            }
            else
            {
                node.Next = _head;
                Debug.Assert(_tail != null, nameof(_tail) + " != null");
                _tail.Next = node;
                _tail = node;
            }
            Count++;
        }
        public bool Remove(T data)
        {
            var current = _head;
            Node<T>? previous = null;
 
            if (IsEmpty) return false;
 
            do
            {
                Debug.Assert(current != null, nameof(current) + " != null");
                Debug.Assert(current.Data != null, "current.Data != null");
                if (current.Data.Equals(data))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current == _tail)
                            _tail = previous;
                    }
                    else 
                    {
                        if(Count==1)
                        {
                            _head = _tail = null;
                        }
                        else
                        {
                            _head = current.Next;
                            Debug.Assert(_tail != null, nameof(_tail) + " != null");
                            _tail.Next = current.Next;
                        }
                    }
                    Count--;
                    return true;
                }
 
                previous = current;
                current = current.Next;
            } while (current != _head);
 
            return false;
        }

        private int Count { get; set; }
        private bool IsEmpty => Count == 0;

        public void Clear()
        {
            _head = null;
            _tail = null;
            Count = 0;
        }
 
        public bool Contains(T data)
        {
            var current = _head;
            if (current == null) return false;
            do
            {
                Debug.Assert(current != null, nameof(current) + " != null");
                Debug.Assert(current.Data != null, "current.Data != null");
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            while (current != _head);
            return false;
        }
 
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
 
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            var current = _head;
            do
            {
                if (current == null) continue;
                yield return current.Data;
                current = current.Next;
            }
            while (current != _head);
        }
    }
