﻿using CircularSinglyLinkedList;

var circularList = new CircularSinglyLinkedList<string>();
 
circularList.Add("1");
circularList.Add("2");
circularList.Add("3");
circularList.Add("4");

Console.Write("Кольцевой односвязный список после добавления элементов: ");
foreach (var item in circularList)
    Console.Write($"{item} ");
 
circularList.Remove("3");
Console.WriteLine("\nКольцевой односвязный список после удаления элемента: ");
foreach (var item in circularList)
    Console.Write($"{item} ");
Console.WriteLine($", {circularList.Count()} элемента");

var isPresent = circularList.Contains("2");
Console.WriteLine(isPresent ? "2 присутствует" : "2 отсутствует");

circularList.Clear();

Console.WriteLine(!circularList.Any() ? "Элементы списка удалены" : "Список не пуст");